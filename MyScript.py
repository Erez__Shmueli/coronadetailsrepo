import json
import requests

returnDataTask1 = {}
returnDataTask2 = {}
returnDataTask3 = {}
globalResultData = {}

url = 'https://covid-api.mmediagroup.fr/v1/history'
params = {'status': 'recovered'}
countries_Recovered = requests.get(url, params).json()

countries = []
recovered_last_week = []
recovered_per_capita = []

# key = country
# val = 'All' -> 'dates' -> 'date' -> 'number'
for key, val in countries_Recovered.items():
    # task3
    countries.append(key)
    if "dates" in val["All"]:
        numbers = list(val['All']['dates'].values())
        recovered_last_week.append(numbers[0] - numbers[6])
    else:
        recovered_last_week.append(0)

    # task2
    if "sq_km_area" in val['All']:
        returnDataTask2[key] = numbers[0] / val['All']['sq_km_area'] * 100
    else:
        returnDataTask2[key] = 0

    # task1
    if "population" in val['All']:
        recovered_per_capita.append((numbers[0] - numbers[9]) / val['All']['population'])
    else:
        recovered_per_capita.append(0)





# task1
sortedList1 = sorted(zip(recovered_per_capita, countries), reverse=True)
for i in range(1, 6):
    returnDataTask1[str(i) + ". " + sortedList1[i][1]] = str(
        sortedList1[i][0]) + " people per capita recovered in the last 10 days"

# task3
sortedList3 = sorted(zip(recovered_last_week, countries), reverse=True)
for i in range(1, 11):
    returnDataTask3[str(i) + ". " + sortedList3[i][1]] = str(sortedList3[i][0]) + " people recovered last week"

globalResultData['task1'] = 'The top 5 countries with most recovered per capita, in the last 10 days'
globalResultData['data1'] = returnDataTask1

globalResultData['task2'] = 'The average recovered cases per 100 square KM per country since the pandemic began'
globalResultData['data2'] = returnDataTask2

globalResultData['task3'] = 'The top 10 countries recovered cases in the last week'
globalResultData['data3'] = returnDataTask3

# In AWS Lambda this is return statement
print(globalResultData)

