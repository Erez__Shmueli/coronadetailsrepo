Erez Shmueli - Covid19 Details - Data API:covid-api.mmediagroup.fr/v1
RETURN - Json of 3 tasks.

0. It is advisable to make the request through some platform (maybe POSTMAN) in order to see the output more conveniently.
1. Code - Python3.6
2. Packages - requests, json
3. tasks:  task1 ~ 'The top 5 countries with most recovered per capita, in the last 10 days'  ~ task2 - 'The average recovered cases per 100 square KM per country since the pandemic began' ~ task3 - 'The top 10 countries recovered cases in the last week'	
4. You can run the script from AWS Lambda in the following execution URL : https://b44rw5czs2.execute-api.us-east-2.amazonaws.com/default/CovidDetails
5. I made all the API requests tests with POSTMAN.
6. Source code is attached.

	
